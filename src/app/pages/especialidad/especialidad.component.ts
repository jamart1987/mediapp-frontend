import { EspecialidadService } from './../../_service/especialidad.service';
import { especialidad } from './../../_model/especialidad';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator,  MatSort, MatTableDataSource,  } from '@angular/material';

@Component({
  selector: 'app-especialidad',
  templateUrl: './especialidad.component.html',
  styleUrls: ['./especialidad.component.css']
})
export class EspecialidadComponent implements OnInit {

  lista: especialidad[] = [];
  displayedColumns = ['idEspecialidad', 'nombres','acciones'];
  dataSource: MatTableDataSource<especialidad>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private EspecialidadService: EspecialidadService) { }

  ngOnInit() {
      this.EspecialidadService.getlistarEspecialidad().subscribe(data => {
    this.lista = data;      
  });

  setTimeout(() => {
    this.dataSource = new MatTableDataSource(this.lista);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }, 1000);

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
