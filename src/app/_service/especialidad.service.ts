import { HOST } from './../_shared/var.constant';
import { especialidad } from './../_model/especialidad';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EspecialidadService {

  url: string = `${HOST}/especialidad`;
   
  constructor(private http: HttpClient) { }

  getlistarEspecialidad(){
    return this.http.get<especialidad[]>(`${this.url}/listar`);
  }

}
